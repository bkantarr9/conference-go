#### ACL.py IMPORT REQUEST IMPORT JSON

import requests
import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f'{city} {state}',
        "per_page": 1, 
        }
    url = 'https://www.pexels.com/v1/search'
    res = requests.get(url, params=params, headers=headers)
    content = json.loads(res.content)

    photo = {}
    try:
        photo["picture_url"] = content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        photo["picture_url"] = None
    return photo        


def get_weather_data(city, state):

    # Define URL 
    url_lat_long = 'http://api.openweathermap.org/geo/1.0/direct'
    url_weather_data = 'https://api.openweathermap.org/data/2.5/weather'

    # Define Headers
    headers = {"Authorization":OPEN_WEATHER_API_KEY}

    # Define parameters for lat/lon request
    location_params = {
        "q": f'{city},{state},USA',
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    # Request for lat/lon values
    res1 = requests.get(url_lat_long, params=location_params, headers=headers)
    print(res1.json())

    # Convert JSON str into python values
    content = res1.json()
    lat=content[0]["lat"]
    lon=content[0]["lon"]


    #Define parameters for temp request
    weather_params= {
        "lat":lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }


    # request for temp value

    res2 = requests.get(url_weather_data, params=weather_params, headers=headers)


    # Convert JSON str into python value
    content2 = res2.json()
    print(res2.json())
    
    temp_kelvin = content2["main"]["temp"]
    temp_fahrenheit = 9/5 * (temp_kelvin - 273) + 32


    # Create weather_data dictionary
    weather_data = {}
    weather_data["temperature"] = temp_fahrenheit
    weather_data["description"] = content2["weather"][0]["description"]

    
    
    # REturn created weather_data
    return weather_data








   










